package ru.tsc.goloshchapov.tm.service;

import ru.tsc.goloshchapov.tm.api.repository.IProjectRepository;
import ru.tsc.goloshchapov.tm.api.service.IProjectService;
import ru.tsc.goloshchapov.tm.api.service.IProjectTaskService;
import ru.tsc.goloshchapov.tm.enumerated.Status;
import ru.tsc.goloshchapov.tm.exception.empty.*;
import ru.tsc.goloshchapov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.goloshchapov.tm.model.Project;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectTaskService projectTaskService;

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectTaskService projectTaskService, IProjectRepository projectRepository) {
        this.projectTaskService = projectTaskService;
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(userId, project);
    }

    @Override
    public void create(final String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(userId, project);
    }

    @Override
    public void add(final String userId, final Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.add(userId, project);
    }

    @Override
    public void remove(final String userId, final Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.remove(userId, project);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) return false;
        return projectRepository.existsById(userId, id);
    }

    @Override
    public boolean existsByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) return false;
        return projectRepository.existsByIndex(userId, index);
    }

    @Override
    public List<Project> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return projectRepository.findAll(userId);
    }

    @Override
    public List<Project> findAll(final String userId, final Comparator<Project> comparator) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (comparator == null) return Collections.emptyList();
        return projectRepository.findAll(userId, comparator);
    }

    @Override
    public Project findById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findById(userId, id);
    }

    @Override
    public Project findByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findByName(userId, name);
    }

    @Override
    public Project findByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return projectRepository.findByIndex(userId, index);
    }

    @Override
    public Project removeById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectTaskService.removeById(userId, id);
    }

    @Override
    public Project removeByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Project project = projectRepository.findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        return projectTaskService.removeById(userId, project.getId());
    }

    @Override
    public Project removeByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        Project project = projectRepository.findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        return projectTaskService.removeById(userId, project.getId());
    }

    @Override
    public Project updateById(final String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final String userId, final Integer index, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.startById(userId, id);
    }

    @Override
    public Project startByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return projectRepository.removeByIndex(userId, index);
    }

    @Override
    public Project startByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.startByName(userId, name);
    }

    @Override
    public Project finishById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findById(userId, id);
    }

    @Override
    public Project finishByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return projectRepository.findByIndex(userId, index);
    }

    @Override
    public Project finishByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.finishByName(userId, name);
    }

    @Override
    public Project changeStatusById(final String userId, final String id, final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        return projectRepository.changeStatusById(userId, id, status);
    }

    @Override
    public Project changeStatusByIndex(final String userId, final Integer index, final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) throw new EmptyStatusException();
        return projectRepository.changeStatusByIndex(userId, index, status);
    }

    @Override
    public Project changeStatusByName(final String userId, final String name, final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        return projectRepository.changeStatusByName(userId, name, status);
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        projectRepository.clear(userId);
    }

}
