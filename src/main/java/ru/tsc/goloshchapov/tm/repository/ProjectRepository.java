package ru.tsc.goloshchapov.tm.repository;

import ru.tsc.goloshchapov.tm.api.repository.IProjectRepository;
import ru.tsc.goloshchapov.tm.enumerated.Status;
import ru.tsc.goloshchapov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.goloshchapov.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(final String userId, Project project) {
        project.setUserId(userId);
        projects.add(project);
    }

    @Override
    public void remove(final String userId, final Project project) {
        if (!userId.equals(project.getUserId())) return;
        projects.remove(project);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        final Project project = findById(userId, id);
        return project != null;
    }

    @Override
    public boolean existsByIndex(final String userId, final Integer index) {
        if (index < 0) return false;
        return index < findAll(userId).size();
    }

    @Override
    public List<Project> findAll(final String userId) {
        final List<Project> projectsWithUserId = new ArrayList<>();
        for (final Project project : projects) {
            if (userId.equals(project.getUserId())) projectsWithUserId.add(project);
        }
        return projectsWithUserId;
    }

    @Override
    public List<Project> findAll(final String userId, final Comparator<Project> comparator) {
        final List<Project> sortedProjectsWithUserId = new ArrayList<>();
        for (final Project project : projects) {
            if (userId.equals(project.getUserId())) sortedProjectsWithUserId.add(project);
        }
        sortedProjectsWithUserId.sort(comparator);
        return sortedProjectsWithUserId;
    }

    @Override
    public Project findById(final String userId, final String id) {
        for (Project project : projects) {
            if (!userId.equals(project.getUserId())) continue;
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findByName(final String userId, final String name) {
        for (Project project : projects) {
            if (!userId.equals(project.getUserId())) continue;
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project findByIndex(final String userId, final Integer index) {
        final List<Project> projectsByUserId = findAll(userId);
        return projectsByUserId.get(index);
    }

    @Override
    public Project removeById(final String userId, final String id) {
        final Project project = findById(userId, id);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByName(final String userId, final String name) {
        final Project project = findByName(userId, name);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(final String userId, final Integer index) {
        final Project project = findByIndex(userId, index);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public void clear(final String userId) {
        final List<Project> projectsForRemoveWithUserId = findAll(userId);
        projects.removeAll(projectsForRemoveWithUserId);
    }

    @Override
    public Project startById(final String userId, final String id) {
        final Project project = findById(userId, id);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startByIndex(final String userId, final Integer index) {
        final Project project = findByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startByName(final String userId, final String name) {
        final Project project = findByName(userId, name);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project finishById(final String userId, final String id) {
        final Project project = findById(userId, id);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishByIndex(final String userId, final Integer index) {
        final Project project = findByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishByName(final String userId, final String name) {
        final Project project = findByName(userId, name);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project changeStatusById(final String userId, final String id, final Status status) {
        final Project project = findById(userId, id);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByIndex(final String userId, final Integer index, final Status status) {
        final Project project = findByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByName(final String userId, final String name, final Status status) {
        final Project project = findByName(userId, name);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

}
