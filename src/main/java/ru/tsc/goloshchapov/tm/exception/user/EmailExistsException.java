package ru.tsc.goloshchapov.tm.exception.user;

import ru.tsc.goloshchapov.tm.exception.AbstractException;

public class EmailExistsException extends AbstractException {

    public EmailExistsException() {
        super("Exception! Email is already exists!");
    }

}
