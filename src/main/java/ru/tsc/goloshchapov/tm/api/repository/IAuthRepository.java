package ru.tsc.goloshchapov.tm.api.repository;

import ru.tsc.goloshchapov.tm.model.User;

public interface IAuthRepository {

    String getUserId();

    void setUserId(String userId);

    boolean isUserIdExists();

    void clearUserId();

}
