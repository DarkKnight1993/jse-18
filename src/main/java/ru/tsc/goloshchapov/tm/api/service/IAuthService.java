package ru.tsc.goloshchapov.tm.api.service;

import ru.tsc.goloshchapov.tm.model.User;

public interface IAuthService {

    User getUser();

    String getUserId();

    boolean isAuth();

    void logout();

    void login(String login, String password);

    void registry(String login, String password, String email);

    void updateProfile(String userId, String firstName, String middleName, String lastName);

    void changePassword(String userId, String password);

}
