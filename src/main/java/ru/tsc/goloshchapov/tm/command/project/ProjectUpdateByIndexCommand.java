package ru.tsc.goloshchapov.tm.command.project;

import ru.tsc.goloshchapov.tm.command.AbstractProjectCommand;
import ru.tsc.goloshchapov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.goloshchapov.tm.model.Project;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

public class ProjectUpdateByIndexCommand extends AbstractProjectCommand {
    @Override
    public String name() {
        return "project-update-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update project by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (!serviceLocator.getProjectService().existsByIndex(userId, index)) throw new ProjectNotFoundException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = serviceLocator.getProjectService().updateByIndex(userId, index, name, description);
        if (projectUpdated == null) throw new ProjectNotFoundException();
        System.out.println("[OK]");
    }

}
