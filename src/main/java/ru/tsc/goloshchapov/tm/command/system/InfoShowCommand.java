package ru.tsc.goloshchapov.tm.command.system;

import ru.tsc.goloshchapov.tm.command.AbstractSystemCommand;
import ru.tsc.goloshchapov.tm.util.NumberUtil;

public class InfoShowCommand extends AbstractSystemCommand {
    @Override
    public String name() {
        return null;
    }

    @Override
    public String arg() {
        return "-i";
    }

    @Override
    public String description() {
        return "Display system information";
    }

    @Override
    public void execute() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory: " + NumberUtil.formatBytes(usedMemory));
    }

}
