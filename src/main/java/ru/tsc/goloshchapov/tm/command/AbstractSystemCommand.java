package ru.tsc.goloshchapov.tm.command;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected void showCommandValue(final String value) {
        if (value == null || value.isEmpty()) return;
        System.out.println(value);
    }

}
